<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User        as User;
use App\Categories as Categories;
use Validator;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {username} {email} {psw}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('username');
        $email = $this->argument('email');
        $psw = $this->argument('psw');

       $validator = Validator::make(
            array(
                'name' => $username,
                'password' =>  $psw,
                'email' => $email
            ),
            array(
                'name' => 'required',
                'password' => 'required|min:6',
                'email' => 'required|email|unique:users'
            )

        ); 

       if ($validator->fails())
            {
                $this->line('Error parameters: UserName Email Psw');

            } else
            {   $user = new User;
                $user->name  = $username;
                $user->email  = $email;
                $user->password = $psw;
                $user->save();     }

            
    }
}
