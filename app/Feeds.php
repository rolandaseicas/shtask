<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feeds extends Model
{
	public $fillable = ['feed', 'title'];

    public function category()
    {
        return $this->belongsTo('App\Categories');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
