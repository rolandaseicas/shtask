<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Categories as Categories;
use App\Feeds as Feeds;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FeedsController extends Controller
{
    public function manageVue()
    {
        return view('manage-vue');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
             $feeds = Feeds::all();
            $categories = Categories::all();
           
            return View::make('feeds', array('feeds' => $feeds, 'categories' => $categories));
    }


    //The method to show the form to add a new feed
  public function showfeeds() {
    
    //$category = Feeds::find(1);
    //echo $category->category->name;
    $feeds = Feeds::all();
    $categories = Categories::all();
   
    return View::make('feeds', array('feeds' => $feeds, 'categories' => $categories));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo 'test'; die();
        $categories = Categories::all();
        $formatedCategories = [];
        foreach($categories as $categorie){
            $formatedCategories[$categorie->id] = $categorie->name;
        }

       // var_dump($categories);
   
        return View::make('feeds.create', array('categories' => $formatedCategories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $feed = new Feeds;
            $feed->feed       = $request->input('feed');
            $feed->title      = $request->input('title');
            $feed->category_id = $request->input('category_id');
            $feed->user_id      = Auth::id();
            $feed->save();
            // redirect
            //Session::flash('message', 'Successfully created feed!');
            
            return redirect('feeds');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categories::all();
        $formatedCategories = [];
         foreach($categories as $categorie){
            $formatedCategories[$categorie->id] = $categorie->name;
        }
        // get the nerd
        $feed = Feeds::find($id);
        //
        // show the edit form and pass the nerd
        //echo $formatedCategories; die();
        return View::make('feeds.edit')
            ->with('feed', $feed)
            ->with('categories', $formatedCategories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function update(Request $request, $id)
    {
       // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'feed'      => 'required'
            //'nerd_level' => 'required|numeric'
        );

      
            // store
            $feed = Feeds::find($id);
            $feed->feed       = $request->input('feed');
            $feed->title      = $request->input('title');
            $feed->category_id = $request->input('category_id');
            $feed->user_id      = Auth::id();
            $feed->save();
            // redirect
           // Session::flash('message', 'Successfully updated nerd!');
            return redirect('feeds');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Feeds::find($id)->delete();
        return redirect('feeds');
    }
}
