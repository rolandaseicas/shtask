<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('home', ['middleware' => 'auth', function () {
//	echo 'helloo ' . Auth::user()->email;
	
//}]);

Route::get('home', function () {
    if(Auth::check()){return Redirect::to('feeds');}
    echo 'helloo ' . Auth::user()->email;
    
});
Route::get('provider/{id}', [
    'middleware' => 'auth',
    'uses' => 'ProviderController@show'
]);


Route::get('/', function () {
   if(Auth::check()){return Redirect::to('feeds');}
    return view('auth/login');
});

Route::get('feeds', [
    'middleware' => 'auth',
    'uses' => 'FeedsController@index'
]);

Route::get('feeds/create', [
    'middleware' => 'auth',
    'uses' => 'FeedsController@create'
]);

Route::post('feeds/store', [
    'middleware' => 'auth',
    'uses' => 'FeedsController@store'
]);

Route::post('category/store', [
    'middleware' => 'auth',
    'uses' => 'CategoriesController@store'
]);

Route::get('feeds/delete/{id}', [
    'middleware' => 'auth',
    'uses' => 'FeedsController@destroy'
]);
Route::get('feeds/edit/{id}', [
    'middleware' => 'auth',
    'uses' => 'FeedsController@edit'
]);

Route::resource('feeds/edit', 'FeedsController');


use Illuminate\Http\Request;

Route::post('newpsw', function (Request $request) {
    $password = $request->input('password');
    
    $user = Auth::user();
	$user->password = $password;
	$user->save();
    $isSaved = $user->save();
      return view('changepsw', ['isSaved' => $isSaved]);

});

//Tried with Vue, but it soesn't worked out so well
//Route::get('manage-vue', 'VueItemController@manageVue');
//Route::resource('vueitems','VueItemController');

use App\Item;

Route::get('users_test', function () {
   $items = Item::paginate(15);
        //$feeds = Feeds::latest()->paginate(5);

        $response = [
           
            'data' => $items
        ];

        return response()->json($items);
});

//Route::post('auth/login', function () {
    //echo 'helloo ' . Auth::user()->email;
    
//});


Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');