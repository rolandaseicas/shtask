@extends ('master')


@section('main_content')



<div class="container">
    <div class="row">
        <div class="col-md-6">
                    
   
              <form class="form-signin" method="POST" action="/auth/login">
              {!! csrf_field() !!}
                <h2 class="form-signin-heading">Please sign in</h2>
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" name="email" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="remember-me"> Remember me
                  </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
              </form>
         </div>   

         <!--  Error handle -->
        @if (count($errors))
    <ul>
        @foreach($errors->all() as $error)
            // Remove the spaces between the brackets
            <li>{!! $error !!}</li>
        @endforeach
    </ul>
@endif
  </div> <!-- /container -->
</div>

     @stop