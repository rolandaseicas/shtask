@extends ('master')


@section('main_content')

<div class="container">

	@if (isset($isSaved))
		  <div class="alert alert-success">
		  	<strong>Success!</strong> New password was saved <a href="feeds" class="alert-link">return to feeds</a>
		</div>
	@else
	    <div class="alert alert-warning">
	    	<strong>Warning!</strong> New password was not saved :/ <a href="feeds" class="alert-link">return to feeds</a>
	  </div>
	@endif
		
		
	</div>


 @stop