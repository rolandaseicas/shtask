@extends ('master')


@section('main_content')
    



  <div class="row">
  </div>
              <!-- /.rows -->
  <div class="row">
      <div class="col-md-2">
                      
       </div>
             
      <div class="col-md-8">
          <div class="panel panel-default">
              <div class="panel-heading" style="min-height: 49px">
                      Feeds
                        <div class="pull-right">  
                            <select onChange="filterCategoryById(this.value)">
                              <option value="0" selected>Categories</option>
                               @foreach($categories as $categorie)
                                   <option value="{{ $categorie->id}}">{{ $categorie->name}}</option>
                                @endforeach
                            </select>
                        </div>
              </div>
                  <!-- /.panel-heaading -->
              <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th>Id</th>
                                  <th>Provider</th>
                                  <th>Title</th>
                                  <th>Feed</th>
                                  <th>Category</th>
                                  <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody id="table_content">
                           @foreach($feeds as $feed)
                              <tr class="category-{{ $feed->category->id }}">
                                  <td>{{ $feed->id }}</td>
                                  <td><a target="_blank" href="provider/{{ $feed->user->id }}">{{ $feed->user->name}}</a></td>
                                  <td><a href="#" data-toggle="modal" data-target="#viewModal-{{ $feed->id }}">{{ $feed->title }} </a></td>
                                  <td>{{ $feed->feed }}</td>
                                  
                                  <td>{{ $feed->category->name }}</td>
                                  <td>
                                    <a href="feeds/delete/{{ $feed->id }}" class="btn btn-danger" > Delete
                                    </a>
                                    <a href="feeds/edit/{{ $feed->id }}" class="btn btn-warning" > Edit
                                    </a>
                                  
                                  </td>
                              </tr>
                           @endforeach
                              
                          </tbody>
                      </table>
                  </div>
                  <!-- /.table-responsive -->
              </div>
              <!-- /.panel-body -->
              </div>
              <!-- /.panel -->
          </div>

  	   <div class="col-md-2">
                      
        </div>
  </div>

   <!-- MODAL Change password -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Change a password</h4>
        </div>
        <div class="modal-body">
          <form action="/newpsw" method="POST">
            <div class="form-group">
              <label for="recipient-name" class="control-label">New password:</label>
              <input type="password" class="form-control" name="password" id="modal_password_id">
            </div>
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Submit new password</button>
            </div>
          </form>
        </div>         
      </div>
    </div>
  </div>
  <!-- /MODAL Change password -->
  <!-- /MODAL Create category -->

    <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="CategoryModalLabel">Create category</h4>
        </div>
        <div class="modal-body">
           <form action="/category/store" method="POST">
                
                <div class="form-group">
                  <label for="recipient-name" class="control-label">Category name:</label>
                  <input type="text" class="form-control" name="category_name" id="text_label">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Create Category</button>
                </div>
              </form>
        </div>         
      </div>
    </div>
  </div>

  <!-- /MODAL Create category -->
  <!-- /MODAL View feed info -->
  @foreach($feeds as $feed)
    <div class="modal fade" id="viewModal-{{ $feed->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">{{ $feed->title }}</h4>
        </div>
        <div class="modal-body">
          {{ $feed->feed }}
          <br>
          <a href="http://{{ $feed->feed }}" target="_blank" class="btn btn-primary">go to feed</a>
        </div>         
      </div>
    </div>
  </div>
  @endforeach

<!-- /MODAL View feed info -->

<script type="text/javascript">

function filterCategoryById(categoryId){
  if (categoryId!='0')  {
      $('tr').hide();
      $('.category-'+categoryId).show();

    } else  
        {   $('tr').show();
       }
}

</script>
 @stop
     <!-- /.row -->