@extends ('master')


@section('main_content')
  <div class="container">  
<td bgcolor="#FFFFFF" style="line-height:30px;" colspan=3>&nbsp;</td>

{!! Form::open(array('url' => 'feeds/store', 'type'=>'post')) !!}

  <div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', Input::old('title'), array('class' => 'form-control')) !!}
  </div>

  <div class="form-group">
    {!! Form::label('feed', 'Feed') !!}
    {!! Form::text('feed', Input::old('feed'), array('class' => 'form-control')) !!}
  </div>

  <div class="form-group">
    {!! Form::label('category_id', 'Category') !!}
    {!! Form::select('category_id', $categories, Input::old('feed_level'), array('class' => 'form-control')) !!}
  </div>

  {!! Form::submit('Create the Feed!', array('class' => 'btn btn-primary')) !!}

{!! Form::close() !!}


</div>
 @stop
    