@extends ('master')


@section('main_content')


<h1>Edit {{ $feed->title }}</h1>

<!-- if there are creation errors, they will show here -->
{!! Html::ul($errors->all()) !!}

{!! Form::model($feed, array('action' => array('FeedsController@update', $feed->id), 'method' => 'PUT')) !!}

  <div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, array('class' => 'form-control')) !!}
  </div>

  <div class="form-group">
    {!! Form::label('feed', 'Feed') !!}
    {!! Form::text('feed', null, array('class' => 'form-control')) !!}
  </div>

  <div class="form-group">
    {!! Form::label('category_id', 'Feed Category') !!}
    {!! Form::select('category_id', $categories, null, array('class' => 'form-control')) !!}
  </div>

  

  {!! Form::submit('Edit Feed', array('class' => 'btn btn-primary')) !!}

{!! Form::close() !!}


 @stop
    