<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>shTask 2</title>

    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap-select.css')}}">

    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>
      <div class="container">
        
            <nav class="navbar navbar-light blue" style="background-color: #e3f2fd;">
              @if (Auth::user())
                <div class="navbar-header">
                  <a class="navbar-brand" href="{{ URL::to('feeds') }}">Feeds</a>
                </div>
                <ul class="nav navbar-nav">
                  <li><a href="{{ URL::to('feeds') }}">View All Feeds</a></li>
                  <li><a href="{{ URL::to('feeds/create') }}">Create a Feed</a>
                  <li><a href="#" data-toggle="modal" data-target="#categoryModal" data-whatever="@fat">Creat Category</a>
                </ul>
                
                 <div class="pull-right">
                   <p class="navbar-text navbar-right">Signed in as &nbsp {{Auth::user()->email}} &nbsp

                     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Change password</button> 
                     <a href="auth/logout" class="btn btn-default" style="margin-right: 30px;" > Log Out
                                                </a>     </p>
                </div>
               @endif
             </nav>
           
    @yield('main_content')



    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
  </body>
</html>